import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output() openSidenav: EventEmitter<boolean>;
  logo = 'assets/images/mpos.avif';

  constructor(
    private router: Router,
  ) {
    this.openSidenav = new EventEmitter();
  }

  ngOnInit(): void {
  }

  open() {
    this.openSidenav.emit(true);
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['/'])
  }

}
