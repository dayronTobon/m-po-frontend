import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { inject } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { FormGenericComponent } from '../form-generic/form-generic.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.scss']
})
export class AddFormComponent implements OnInit {

  @Input() title = 'Crear';
  @Input() configForm?: any;
  @Input() titleButton = 'Guardar';

  @Output() isOpenDialog: EventEmitter<BehaviorSubject<boolean>>;
  @Output() setForm: EventEmitter<FormGroup>;
  @Output() sendData: EventEmitter<any>;

  formDynamic?: FormGroup;
  open$ = new BehaviorSubject<boolean>(true);

  constructor(
    public dialog: MatDialog
  ) {
    this.isOpenDialog = new EventEmitter();
    this.setForm = new EventEmitter();
    this.sendData = new EventEmitter();
  }

  ngOnInit(): void {
    this.isOpenDialog.emit(this.open$);
  }

  openDialogForm() {
    this.open$.next(true);
    const dialogRef = this.dialog.open(FormGenericComponent, {
      data: {
        title: this.title,
        form: this.configForm,
        titleButton: this.titleButton,
        isOpen: this.open$,
      },
      width: '400px',
    });
    dialogRef.componentInstance.formGeneric.subscribe({
      next: (form: FormGroup) => {
        this.formDynamic = form;
        this.setForm.emit(form);
      }
    })
    dialogRef.componentInstance.sendData.subscribe({
      next: (data: any) => {
        this.sendData.emit(data);
      }
    })
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (!result) {
          this.formDynamic?.reset();
        }
      }
    });
  }

}
