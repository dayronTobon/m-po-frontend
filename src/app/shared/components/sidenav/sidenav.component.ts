import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  showFiller = false;
  menuList: any[] = [];
  img = 'assets/images/back.jpg';

  constructor() {}

  ngOnInit(): void {
    this.menuList = JSON.parse(localStorage.getItem('menuAccess') as string);
  }
}
