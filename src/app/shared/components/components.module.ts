import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { GenericTableComponent } from './generic-table/generic-table.component';
import { AddFormComponent } from './add-form/add-form.component';
import { FormGenericComponent } from './form-generic/form-generic.component';
import { TextFieldComponent } from './form-generic/fields/text-field/text-field.component';
import { NumberFieldComponent } from './form-generic/fields/number-field/number-field.component';
import { SelectFieldComponent } from './form-generic/fields/select-field/select-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordFieldComponent } from './form-generic/fields/password-field/password-field.component';
import { NotificationComponent } from './notification/notification.component';

const components = [
  SidenavComponent,
  ToolbarComponent,
  GenericTableComponent,
  AddFormComponent,
  FormGenericComponent,
  TextFieldComponent,
  NumberFieldComponent,
  SelectFieldComponent,
]


@NgModule({
  declarations: [
    ...components,
    PasswordFieldComponent,
    NotificationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports: [
    ...components
  ]
})
export class ComponentsModule { }
