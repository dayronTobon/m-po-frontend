import { ENameValidated } from "../enum/validations.enum";

export interface IField {
  type: 'number' | 'text' | 'select' | 'password';
  name: string;
  label?: string;
  hidden?: boolean,
  onlyEdit?: boolean,
  value: any;
  options?: IOptions[];
  validations?: IValidation[];
  method?: 'POST' | 'GET',
  path?: string;
  modelResponse?: IModelResponse;
}

export interface IOptions {
  name: string;
  value: any;
  allData?: any;
}

export interface IValidation {
  validate: ITypeValidation['type'];
  message: string;
  value?: any;
}

export interface ITypeValidation {
  type:
    ENameValidated.required |
    ENameValidated.maxLength |
    ENameValidated.minLength |
    ENameValidated.max |
    ENameValidated.min |
    ENameValidated.email |
    ENameValidated.pattern |
    ENameValidated.requiredTrue;
}


export interface IModelResponse {
  name: string;
  value: string;
}
