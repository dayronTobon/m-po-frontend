import { Observable } from "rxjs";
import { IForm } from "./form.interface";

export interface IDialogData {
  title: string;
  titleButton: string;
  form: IForm;
  isOpen?: Observable<boolean>;
}
