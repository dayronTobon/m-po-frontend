import { IField } from "./field.interface";

export interface IForm {
  editForm?: boolean;
  dataEdit?: any;
  fields: IField[];
}
