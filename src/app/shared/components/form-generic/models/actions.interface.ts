export interface IActions {
  data: any;
  action: 'edit' | 'active' | 'deactivate' | 'delete';
}
