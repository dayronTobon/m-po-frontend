import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IForm } from './models/form.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormGenericService } from './services/form-generic.service';
import { IDialogData } from './models/dialog-data.interface';

@Component({
  selector: 'app-form-generic',
  templateUrl: './form-generic.component.html',
  styleUrls: ['./form-generic.component.scss'],
})
export class FormGenericComponent implements OnInit {

  @Input() isDialog = true;
  @Input() dataForm?: IDialogData;
  @Output() formGeneric: EventEmitter<FormGroup>;
  @Output() sendData: EventEmitter<any>;
  title = 'Crear';
  fromModule?: IForm;
  titleButton = 'Guardar';
  dynamicForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<FormGenericComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData,
    private formGenericService: FormGenericService
  ) {
    this.formGeneric = new EventEmitter();
    this.sendData = new EventEmitter();
    this.dynamicForm = this.fb.group({});
  }

  ngOnInit(): void {
    if (this.isDialog) {
      this.title = this.data.title;
      this.fromModule = this.data.form;
      this.titleButton = this.data.titleButton;
      if(this.data.isOpen) {
        this.data.isOpen.subscribe({
          next: (isOpen: boolean) => {
            if (!isOpen) {
              this.dynamicForm.reset();
              this.dialogRef.close(true);
            }
          },
        });
      }
    } else if (this.dataForm) {
      this.title = this.dataForm.title;
      this.fromModule = this.dataForm.form;
      this.titleButton = this.dataForm.titleButton;
    }
    this.setForm();
  }

  setForm() {
    this.dynamicForm = this.formGenericService.buildForm(
      this.fromModule?.fields || [],
      this.fromModule?.editForm || false,
      this.fromModule?.dataEdit
    );
    this.formGeneric.emit(this.dynamicForm);
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

  sendDataForm(evt: Event) {
    evt.preventDefault();
    if (!this.dynamicForm.valid) {
      return;
    }
    // this.dialogRef.close(this.dynamicForm.value);
    this.sendData.emit(this.dynamicForm.value);
  }
}
