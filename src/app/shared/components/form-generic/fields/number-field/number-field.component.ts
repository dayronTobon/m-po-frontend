import { Component, Input, OnInit } from '@angular/core';
import { IField } from '../../models/field.interface';

@Component({
  selector: 'app-number-field',
  templateUrl: './number-field.component.html',
  styleUrls: ['./number-field.component.scss']
})
export class NumberFieldComponent implements OnInit {

  @Input() field?: IField;
  @Input() fieldControl: any;

  get touched(): boolean {
    if (this.fieldControl) {
      return this.fieldControl?.touched;
    }
    return false;
  }

  get isInvalid(): boolean {
    if (this.fieldControl) {
      return this.fieldControl?.invalid && this.fieldControl?.touched;
    }
    return false;
  }

  get errorMessage(): string {
    if (this.field && this.field.validations) {
      for(const validated of this.field.validations){
        if (this.fieldControl?.hasError(validated.validate) && this.fieldControl?.touched) {
          return validated.message;
        }
      }
    }
    return '';
  }

  constructor() { }

  ngOnInit(): void {
  }

}
