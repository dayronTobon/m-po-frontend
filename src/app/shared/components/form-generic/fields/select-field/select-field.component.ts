import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IField, IOptions } from '../../models/field.interface';
import { FieldsService } from '../../services/fields.service';

@Component({
  selector: 'app-select-field',
  templateUrl: './select-field.component.html',
  styleUrls: ['./select-field.component.scss']
})
export class SelectFieldComponent implements OnInit, OnChanges {

  @Input() field?: IField;
  @Input() fieldControl: any;

  get touched(): boolean {
    if (this.fieldControl) {
      return this.fieldControl?.touched;
    }
    return false;
  }

  get isInvalid(): boolean {
    if (this.fieldControl) {
      return this.fieldControl?.invalid && this.fieldControl?.touched;
    }
    return false;
  }

  get errorMessage(): string {
    if (this.field && this.field.validations) {
      for(const validated of this.field.validations){
        if (this.fieldControl?.hasError(validated.validate) && this.fieldControl?.touched) {
          return validated.message;
        }
      }
    }
    return '';
  }

  constructor(
    private fieldsService: FieldsService,
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['field']) {
      const value = changes['field'].currentValue;
      if(value) {
        this.getListOptions();
      }
    }
  }

  getListOptions() {
    if (this.field?.path && this.field.method) {
      this.fieldsService.getList(this.field?.path || '').subscribe({
        next: (res: any) => {
          this.field?.options?.splice(0, this.field?.options?.length);
          const dataOptions: IOptions[] = res.data.list.map((item: any) => {
            let dataOptions = {
              name: item[this.field?.modelResponse?.name || ''],
              value: item[this.field?.modelResponse?.value || ''],
              allData: item
            }
            return dataOptions;
          })
          this.field?.options?.push(...dataOptions);
        }
      })
    }
  }

}
