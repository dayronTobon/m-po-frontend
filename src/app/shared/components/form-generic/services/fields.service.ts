import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FieldsService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  getList(path: string) {
    return this.http.get(`${this.baseUrl}/${path}`);
  }

}
