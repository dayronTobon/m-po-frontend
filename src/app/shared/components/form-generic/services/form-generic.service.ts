import { Injectable } from '@angular/core';
import { IField, ITypeValidation, IValidation } from '../models/field.interface';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ENameValidated } from '../enum/validations.enum';

@Injectable({
  providedIn: 'root'
})
export class FormGenericService {

  formTemp: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.formTemp = this.fb.group({});
  }

  buildForm(fields: IField[], isEdit: boolean, dataEdit: any): FormGroup {
    this.formTemp = this.fb.group({});
    fields.forEach(field  => {
      if(field.onlyEdit) {
        if (isEdit) {
          this.setControl(field, isEdit, dataEdit);
        }
      } else {
        this.setControl(field, isEdit, dataEdit);
      }
      return field;
    });
    return this.formTemp;
  }

  setControl(field: IField, isEdit: boolean, dataEdit: any) {
    const validators = field.validations ?  this.setValidations(field.validations) : null;
    const value = !isEdit ? field.value : dataEdit[field.name];
    const control = new FormControl(value, validators);
    this.formTemp?.addControl(field.name, control);
  }

  setValidations(validations: IValidation[]): ValidatorFn | ValidatorFn[] | null | undefined {
    if (validations.length > 0) {
      const listValidations: ValidatorFn[] = [];
      for(const validation of validations) {
        const getValidate = this.getValidation(validation.validate, validation.value);
        listValidations.push(getValidate);
      }
      return listValidations.length > 0 ? listValidations : null;
    }
    return undefined;
  }

  getValidation(validation: ITypeValidation['type'], value?: any): ValidatorFn {
    switch (validation) {
      case ENameValidated.required:
        return Validators.required;
      case ENameValidated.requiredTrue:
        return Validators.requiredTrue;
      case ENameValidated.email:
        return Validators.email;
      case ENameValidated.minLength:
        return Validators.minLength(value);
      case ENameValidated.maxLength:
        return Validators.maxLength(value);
      case ENameValidated.max:
        return Validators.max(value);
      case ENameValidated.min:
        return Validators.min(value);
      case ENameValidated.pattern:
        return Validators.pattern(value);
      default:
        return Validators.required;
    }
  }


}
