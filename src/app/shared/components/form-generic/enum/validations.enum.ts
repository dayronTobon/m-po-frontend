export enum ENameValidated {
  required = 'required',
  maxLength = 'maxlength',
  minLength = 'minlength',
  email = 'email',
  min = 'min',
  max = 'max',
  pattern = 'pattern',
  requiredTrue = 'requiredtrue',
}
