import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, _MatTableDataSource } from '@angular/material/table';
import { IActions } from '../form-generic/models/actions.interface';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.scss']
})
export class GenericTableComponent implements OnInit {

  @Input() title: string = '';
  @Input() columns: any[] = [];
  @Input() dataTable: any[] = [];
  @Input() elevationWhitPagination = false;
  @Input() showPagination = false;
  @Input() pageSize: number = 0;
  @Input() pageIndex: number = 0;
  @Input() pageLength: number = 0;
  @Input() loadign = false;

  @Output() selected: EventEmitter<any>;
  @Output() changePagination: EventEmitter<MatPaginator>;
  @Output() changeDataRow: EventEmitter<IActions>;

  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor() {
    this.selected = new EventEmitter();
    this.changePagination = new EventEmitter();
    this.changeDataRow = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (changes.dataTable) {
      this.dataSource = new MatTableDataSource<any>([]);
      const dataTable: any[] = changes.dataTable.currentValue;
      if (dataTable.length > 0) {
        this.dataSource = new MatTableDataSource(dataTable);
      }
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  selectedRow(rowData: any) {
    this.selected.emit(rowData);
  }

  page(page: any){
    this.changePagination.emit(page);
  }

  checkValueView(row: any, view: any[]) {
    return view.map(item => row[item?.field] === item?.value).every(el => el);
  }

  activeAction(data: any, action: IActions['action']) {
    this.changeDataRow.emit({
      data: data,
      action: action,
    });
  }

}
