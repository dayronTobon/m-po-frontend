import { Injectable } from '@angular/core';
import { INotification } from '../models/notification.interface';
import { NotificationComponent } from '../notification.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private snackBar: MatSnackBar
  ) {}

  showNotification(message: string, type: INotification['type'] = 'success') {
    this.show({
      content: { message },
      type,
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    })
  }

  show({
    content,
    type,
    duration=50000,
    verticalPosition="top",
    horizontalPosition="right",
  }: INotification) {
    this.snackBar.openFromComponent(NotificationComponent, {
      data: {
        message: content.message,
        icon: type,
      },
      verticalPosition,
      horizontalPosition,
      panelClass: `${type}-notification`,
      duration
    });
  }

}
