import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from "@angular/material/snack-bar";

export interface INotification {
  content: IContent;
  type: 'error' | 'success' | 'info' | 'warning';
  duration?: number;
  verticalPosition?: MatSnackBarVerticalPosition;
  horizontalPosition?: MatSnackBarHorizontalPosition;
}

export interface IContent {
  title?: string;
  message: string;
}

export interface IDataNotification {
  message: string;
  icon: 'error' | 'success' | 'info' | 'warning';
}
