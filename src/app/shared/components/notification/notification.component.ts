import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { IDataNotification } from './models/notification.interface';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {

  icon = {
    success: 'task_alt',
    error: 'report',
    info: 'info',
    warning: 'warning',
  }

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: IDataNotification,
    public snackbarRef: MatSnackBarRef<NotificationComponent>
  ) {}

}
