import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ConfigHttpInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (request.headers.get("skip")) {
      return next.handle(request);
    }
    const token = localStorage.getItem('token');
    const contentType = request.headers.get('Content-Type');
    const req = request.clone({
      headers: new HttpHeaders({
        'Content-Type':  contentType ? contentType : 'application/json',
        'Accept': 'application/json',
        'authorization': `Bearer ${token}`,
        'Cache-Control': 'no-cache',
      })
    });
    return next.handle(req);
  }
}
