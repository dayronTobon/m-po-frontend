import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  statusLogin(): boolean {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    if (token.length < 2) {
      return false;
    }
    const expiredDate = localStorage.getItem('expiresIn');
    const today = new Date();
    const expiresIn = Number(expiredDate)
    const expiresInDate = new Date();
    expiresInDate.setTime(expiresIn + expiresInDate.getTime());
    if (expiresInDate > today) {
      return true;
    }else {
      return false;
    }
  }

  isAdmin() {
    const rol = localStorage.getItem('rol');
    return rol == 'ADMIN'
  }
}
