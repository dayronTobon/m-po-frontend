import { Component, OnInit } from '@angular/core';
import { IDialogData } from 'src/app/shared/components/form-generic/models/dialog-data.interface';
import { loginForm } from './config/login-config-form';
import { FormGroup } from '@angular/forms';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logo = 'assets/images/mpos.avif';
  img = 'assets/images/back.jpg';
  dataForm: IDialogData = {
    title: 'Iniciar Sessión',
    form: loginForm,
    titleButton: 'Iniciar Sesión',
  }
  loginForm?: FormGroup;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
  }

  setForm(form: FormGroup) {
    this.loginForm = form;
  }

  login(data: any) {
    this.loginService.login(data).subscribe({
      next: (res: any) => {
        this.loginForm?.reset();
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('rol', res.data.rol);
        localStorage.setItem('expiresIn', res.data.expiresIn);
        localStorage.setItem('menuAccess', JSON.stringify(res.data.menuAccess));
        this.router.navigate(['/app'])
      },
      error: (err) => {
        if (err.error.code == 401) {
          this.notificationService.showNotification(`Compruebe su nombre de usuario o contraseña`, 'error');
        }else {
          this.notificationService.showNotification(`¡Ocurrio un ERROR al intentar iniciar sesión!`, 'error');
        }
      }
    })
  }

}
