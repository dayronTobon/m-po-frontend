import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  login(dataLogin: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/auth`, dataLogin, {headers:{skip:"true"}});
  }
}
