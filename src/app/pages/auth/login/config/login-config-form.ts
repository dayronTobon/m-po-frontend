import { ENameValidated } from "../../../../shared/components/form-generic/enum/validations.enum";
import { IForm } from "../../../../shared/components/form-generic/models/form.interface";

export const loginForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'username',
      label: 'Nombre de usuario',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    },
    {
      type: 'password',
      name: 'password',
      label: 'Contraseña',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    }
  ]
}
