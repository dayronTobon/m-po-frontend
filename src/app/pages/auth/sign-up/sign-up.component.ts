import { Component, OnInit } from '@angular/core';
import { IDialogData } from 'src/app/shared/components/form-generic/models/dialog-data.interface';
import { registerForm } from './config/sign-up-config-form';
import { FormGroup } from '@angular/forms';
import { UserService } from '../../modules/users/services/user.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  logo = 'assets/images/mpos.avif';
  img = 'assets/images/back.jpg';
  dataForm: IDialogData = {
    title: 'Regístrate',
    form: registerForm,
    titleButton: 'Regístrate',
  }
  signUpForm?: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
  }

  setForm(form: FormGroup) {
    this.signUpForm = form;
  }

  signUp(data: any) {
    this.userService.createUser(data).subscribe({
      next: (res) => {
        this.signUpForm?.reset();
        this.notificationService.showNotification(`Se creo exitosamente el usuario`);
        this.router.navigate(['/'])
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al intentar  crear el usuario!`, 'error');
      }
    })
  }

}
