import { ENameValidated } from "../../../../shared/components/form-generic/enum/validations.enum";
import { IForm } from "../../../../shared/components/form-generic/models/form.interface";

export const registerForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'full_name',
      label: 'Nombre completo',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    },
    {
      type: 'text',
      name: 'username',
      label: 'Nombre de usuario',
      value: '',
    },
    {
      type: 'password',
      name: 'password',
      label: 'Contraseña',
      value: '',
    },
  ]
}
