import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  getListCategories(): Observable<Object> {
    return this.http.get(`${this.baseUrl}/category`)
  }

  getCategory(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/category/${id}`);
  }

  createCategory(dataCategory: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/category`, dataCategory);
  }

  editCategory(data: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/category/${data.id}`, data);
  }

  cahngeStatusCategory(id: string, status: boolean): Observable<Object> {
    return this.http.patch(`${this.baseUrl}/category/status/${id}`, { status: status});
  }

  deleteCategory(id: string): Observable<Object> {
    return this.http.delete (`${this.baseUrl}/category/${id}`);
  }

}
