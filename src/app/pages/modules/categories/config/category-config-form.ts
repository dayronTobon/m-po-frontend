import { ENameValidated } from "../../../../shared/components/form-generic/enum/validations.enum";
import { IForm } from "../../../../shared/components/form-generic/models/form.interface";

export const CategoryForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'id',
      value: '',
      hidden: true,
      onlyEdit: true,
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo requerido',
        },
      ],
    },
    {
      type: 'text',
      name: 'name',
      label: 'Nombre de categoria',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    }
  ]
}
