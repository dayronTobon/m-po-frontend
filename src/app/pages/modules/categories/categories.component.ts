import { Component, OnInit } from '@angular/core';
import { CategoryService } from './services/category.service';
import { CategoryForm } from './config/category-config-form';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { IActions } from '../../../shared/components/form-generic/models/actions.interface';
import { FormGenericComponent } from '../../../shared/components/form-generic/form-generic.component';
import { IForm } from '../../../shared/components/form-generic/models/form.interface';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  titleTable = 'Categorías';
  columns: any[] = [];
  dataTable: any[] = [];
  loading = false;
  titleForm = 'Crear categoria';
  categoryForm = CategoryForm;
  formGroupCatrory?: FormGroup;
  isOpenDialog$?: BehaviorSubject<boolean>;

  constructor(
    public dialog: MatDialog,
    private categoryService: CategoryService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.getListCategories();
  }

  changeDataRow(evt: IActions) {
    const activeAction = {
      'edit': (row: any, status: boolean) => this.editCategory(row),
      'active': (row: any, status: boolean) => this.changeStatusCategory(row, status),
      'deactivate': (row: any, status: boolean) => this.changeStatusCategory(row, status),
      'delete': (row: any, status: boolean) => this.deleteCategory(row),
    }
    activeAction[evt.action]?.(evt.data, evt.action == 'active');
  }

  getListCategories() {
    this.loading = true;
    this.categoryService.getListCategories().subscribe({
      next: (res: any) => {
        this.loading = false;
        this.dataTable = res.data.list;
        this.columns = res.data.configColumns;
      },
      error: (err) => {
        this.loading = false;
      }
    })
  }

  sendData(data: any) {
    this.categoryService.createCategory(data).subscribe({
      next: (res) => {
        this.isOpenDialog$?.next(false);
        this.notificationService.showNotification(`Se creo exitosamente la categoria`);
        this.getListCategories();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al guardar los datos!`, 'error');
      }
    })
  }

  editCategory(data: any) {
    this.categoryService.getCategory(data.id).subscribe({
      next: (findCategory) => {
        this.openDialogEdit(findCategory);
      }
    })
  }

  openDialogEdit(findCategory: any) {
    const formEdition: IForm = JSON.parse(JSON.stringify(this.categoryForm));
    formEdition.editForm = true;
    formEdition.dataEdit = findCategory.data;
    this.isOpenDialog$?.next(true);
    const dialogRef = this.dialog.open(FormGenericComponent, {
      data: {
        title: 'Actualizar Categoria',
        form: formEdition,
        titleButton: 'Actualizar',
        isOpen: this.isOpenDialog$,
      },
      width: '400px',
    });
    dialogRef.componentInstance.formGeneric.subscribe({
      next: (form: FormGroup) => {
        this.formGroupCatrory = form;
      }
    })
    dialogRef.componentInstance.sendData.subscribe({
      next: (data: any) => {
        this.updateProdut(data);
      }
    })
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (!result) {
          this.formGroupCatrory?.reset();
        }
      }
    });
  }

  updateProdut(data: any) {
    this.categoryService.editCategory(data).subscribe({
      next: (update) => {
        this.getListCategories();
        this.notificationService.showNotification(`Se actualizo exitosamente la categoria`);
        this.isOpenDialog$?.next(false);
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al actualizar los datos!`, 'error');
      }
    })
  }

  changeStatusCategory(data: any, status: boolean) {
    this.categoryService.cahngeStatusCategory(data.id, status).subscribe({
      next: (change) => {
        this.notificationService.showNotification(`Cambio exitosamente el estado de la categoria`);
        this.getListCategories();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al cambiar el estado!`, 'error');
      }
    })
  }

  deleteCategory(data: any) {
    this.categoryService.deleteCategory(data.id).subscribe({
      next: (resDelete) => {
        this.notificationService.showNotification(`Se elimino exitosamente la categoria`, 'info');
        this.getListCategories();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al eliminar los datos!`, 'error');
      }
    })
  }

  isOpenObserver(isOpen: BehaviorSubject<boolean>) {
    this.isOpenDialog$ = isOpen;
  }

  setForm(form: FormGroup) {
    this.formGroupCatrory = form;
  }

}
