import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulesRoutingModule } from './modules-routing.module';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { CategoriesComponent } from './categories/categories.component';
import { UsersComponent } from './users/users.component';
import { ListTagsComponent } from './list-tags/list-tags.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
    ProductsComponent,
    CategoriesComponent,
    UsersComponent,
    ListTagsComponent
  ],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    SharedModule,
  ],
})
export class ModulesModule { }
