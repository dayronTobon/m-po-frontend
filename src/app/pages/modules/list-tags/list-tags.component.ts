import { Component, OnInit } from '@angular/core';
import { listTagsForm } from './config/list-tags-config-form';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ListTagsService } from './services/list-tags.service';
import { IActions } from '../../../shared/components/form-generic/models/actions.interface';
import { FormGenericComponent } from '../../../shared/components/form-generic/form-generic.component';
import { IForm } from 'src/app/shared/components/form-generic/models/form.interface';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-list-tags',
  templateUrl: './list-tags.component.html',
  styleUrls: ['./list-tags.component.scss']
})
export class ListTagsComponent implements OnInit {

  titleTable = 'List tags';
  columns: any[] = [];
  dataTable: any[] = [];
  loading = false;
  titleForm = 'Crear list tags';
  listTagsForm = listTagsForm;
  formGroupListTags?: FormGroup;
  isOpenDialog$?: BehaviorSubject<boolean>;

  constructor(
    public dialog: MatDialog,
    private listTagsService: ListTagsService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.getAllListTags();
  }

  changeDataRow(evt: IActions) {
    const activeAction = {
      'edit': (row: any, status: boolean) => this.editListTags(row),
      'active': (row: any, status: boolean) => this.changeStatusListTags(row, status),
      'deactivate': (row: any, status: boolean) => this.changeStatusListTags(row, status),
      'delete': (row: any, status: boolean) => this.deleteListTags(row),
    }
    activeAction[evt.action]?.(evt.data, evt.action == 'active');
  }

  getAllListTags() {
    this.loading = true;
    this.listTagsService.getAllListTags().subscribe({
      next: (res: any) => {
        this.loading = false;
        this.dataTable = res.data.list;
        this.columns = res.data.configColumns;
      },
      error: (err) => {
        this.loading = false;
      }
    })
  }

  sendData(data: any) {
    this.listTagsService.createListTags(data).subscribe({
      next: (res) => {
        this.isOpenDialog$?.next(false);
        this.notificationService.showNotification(`Se creo exitosamente la etiqueta`);
        this.getAllListTags();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al guardar los datos!`, 'error');
      }
    })
  }

  editListTags(data: any) {
    this.listTagsService.getListTags(data.id).subscribe({
      next: (findListTags) => {
        this.openDialogEdit(findListTags);
      }
    })
  }

  openDialogEdit(findListTags: any) {
    const formEdition: IForm = JSON.parse(JSON.stringify(this.listTagsForm));
    formEdition.editForm = true;
    formEdition.dataEdit = findListTags.data;
    this.isOpenDialog$?.next(true);
    const dialogRef = this.dialog.open(FormGenericComponent, {
      data: {
        title: 'Actualizar Lista de Etiquetas',
        form: formEdition,
        titleButton: 'Actualizar',
        isOpen: this.isOpenDialog$,
      },
      width: '400px',
    });
    dialogRef.componentInstance.formGeneric.subscribe({
      next: (form: FormGroup) => {
        this.formGroupListTags = form;
      }
    })
    dialogRef.componentInstance.sendData.subscribe({
      next: (data: any) => {
        this.updateProdut(data);
      }
    })
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (!result) {
          this.formGroupListTags?.reset();
        }
      }
    });
  }

  updateProdut(data: any) {
    this.listTagsService.editListTags(data).subscribe({
      next: (update) => {
        this.getAllListTags();
        this.notificationService.showNotification(`Se actualizo exitosamente la etiqueta`);
        this.isOpenDialog$?.next(false);
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al actualizar los datos!`, 'error');
      }
    })
  }

  changeStatusListTags(data: any, status: boolean) {
    this.listTagsService.cahngeStatusListTags(data.id, status).subscribe({
      next: (change) => {
        this.notificationService.showNotification(`Cambio exitosamente el estado de la etiqueta`);
        this.getAllListTags();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al cambiar el estado!`, 'error');
      }
    })
  }

  deleteListTags(data: any) {
    this.listTagsService.deleteListTags(data.id).subscribe({
      next: (resDelete) => {
        this.notificationService.showNotification(`Se elimino exitosamente la etiqueta`, 'info');
        this.getAllListTags();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al eliminar los datos!`, 'error');
      }
    })
  }

  isOpenObserver(isOpen: BehaviorSubject<boolean>) {
    this.isOpenDialog$ = isOpen;
  }

  setForm(form: FormGroup) {
    this.formGroupListTags = form;
  }

}
