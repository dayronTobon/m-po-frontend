import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListTagsService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  getAllListTags(): Observable<Object> {
    return this.http.get(`${this.baseUrl}/list-tags`)
  }

  getListTags(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/list-tags/${id}`);
  }

  createListTags(dataListTags: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/list-tags`, dataListTags);
  }

  editListTags(data: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/list-tags/${data.id}`, data);
  }

  cahngeStatusListTags(id: string, status: boolean): Observable<Object> {
    return this.http.patch(`${this.baseUrl}/list-tags/status/${id}`, { status: status});
  }

  deleteListTags(id: string): Observable<Object> {
    return this.http.delete (`${this.baseUrl}/list-tags/${id}`);
  }

}
