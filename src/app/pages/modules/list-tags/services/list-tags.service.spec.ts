import { TestBed } from '@angular/core/testing';

import { ListTagsService } from './list-tags.service';

describe('ListTagsService', () => {
  let service: ListTagsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListTagsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
