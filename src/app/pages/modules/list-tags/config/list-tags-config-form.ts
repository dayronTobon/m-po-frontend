import { ENameValidated } from "../../../../shared/components/form-generic/enum/validations.enum";
import { IForm } from "../../../../shared/components/form-generic/models/form.interface";

export const listTagsForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'id',
      value: '',
      hidden: true,
      onlyEdit: true,
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo requerido',
        },
      ],
    },
    {
      type: 'select',
      name: 'id_product',
      label: 'Seleciona Producto',
      value: null,
      options: [],
      method: 'GET',
      path: 'product',
      modelResponse: {
        value: 'id',
        name: 'name',
      },
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo producto es requerido',
        }
      ]
    },
    {
      type: 'text',
      name: 'value',
      label: 'Etiqueta',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo etiqueta es requerido',
        }
      ]
    },
    {
      type: 'text',
      name: 'description',
      label: 'Descripcción',
      value: '',
    },
  ]
}
