import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './categories/categories.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { ListTagsComponent } from './list-tags/list-tags.component';
import { RolGuard } from 'src/app/core/guards/rol.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'category', pathMatch: 'full' },
      {
        path: 'user',
        canActivate: [RolGuard],
        component: UsersComponent
      },
      {
        path: 'category',
        component: CategoriesComponent,
      },
      {
        path: 'product',
        component: ProductsComponent,
      },
      {
        path: 'list-tags',
        component: ListTagsComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
