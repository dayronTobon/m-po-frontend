import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  getListUsers(): Observable<Object> {
    return this.http.get(`${this.baseUrl}/user`)
  }

  getUser(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/user/${id}`);
  }

  createUser(dataUser: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/user`, dataUser, {headers:{skip:"true"}} );
  }

  editUser(data: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/user/${data.id}`, data);
  }

  cahngeStatusUser(id: string, status: boolean): Observable<Object> {
    return this.http.patch(`${this.baseUrl}/user/status/${id}`, { status: status});
  }

  deleteUser(id: string): Observable<Object> {
    return this.http.delete (`${this.baseUrl}/user/${id}`);
  }

}
