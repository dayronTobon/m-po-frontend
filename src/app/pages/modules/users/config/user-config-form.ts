import { ENameValidated } from "../../../../shared/components/form-generic/enum/validations.enum";
import { IForm } from "../../../../shared/components/form-generic/models/form.interface";

export const userForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'id',
      value: '',
      hidden: true,
      onlyEdit: true,
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo requerido',
        },
      ],
    },
    {
      type: 'text',
      name: 'full_name',
      label: 'Nombre completo',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    },
    {
      type: 'text',
      name: 'username',
      label: 'Nombre de usuario',
      value: '',
    },
    {
      type: 'password',
      name: 'password',
      label: 'Contraseña',
      value: '',
    },
    {
      type: 'select',
      name: 'rol',
      label: 'Seleciona Rol',
      value: 'USER',
      options: [
        {
          value: 'ADMIN',
          name: 'ADMIN',
        },
        {
          value: 'USER',
          name: 'USER',
        }
      ]
    }
  ]
}

export const userEditForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'id',
      value: '',
      hidden: true,
      onlyEdit: true,
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo requerido',
        },
      ],
    },
    {
      type: 'text',
      name: 'full_name',
      label: 'Nombre completo',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        }
      ]
    },
    {
      type: 'text',
      name: 'username',
      label: 'Nombre de usuario',
      value: '',
    },
    {
      type: 'select',
      name: 'rol',
      label: 'Seleciona Rol',
      value: 'USER',
      options: [
        {
          value: 'ADMIN',
          name: 'ADMIN',
        },
        {
          value: 'USER',
          name: 'USER',
        }
      ]
    }
  ]
}

