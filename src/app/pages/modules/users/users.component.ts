import { Component, OnInit } from '@angular/core';
import { userEditForm, userForm } from './config/user-config-form';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { IActions } from 'src/app/shared/components/form-generic/models/actions.interface';
import { IForm } from 'src/app/shared/components/form-generic/models/form.interface';
import { FormGenericComponent } from 'src/app/shared/components/form-generic/form-generic.component';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  titleTable = 'Usuarios';
  columns: any[] = [];
  dataTable: any[] = [];
  loading = false;
  titleForm = 'Crear usuario';
  userForm = userForm;
  userEditForm = userEditForm;
  formGroupUser?: FormGroup;
  isOpenDialog$?: BehaviorSubject<boolean>;

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.getListUsers();
  }

  changeDataRow(evt: IActions) {
    const activeAction = {
      'edit': (row: any, status: boolean) => this.editUser(row),
      'active': (row: any, status: boolean) => this.changeStatusUser(row, status),
      'deactivate': (row: any, status: boolean) => this.changeStatusUser(row, status),
      'delete': (row: any, status: boolean) => this.deleteUser(row),
    }
    activeAction[evt.action]?.(evt.data, evt.action == 'active');
  }

  getListUsers() {
    this.loading = true;
    this.userService.getListUsers().subscribe({
      next: (res: any) => {
        this.loading = false;
        this.dataTable = res.data.list;
        this.columns = res.data.configColumns;
      },
      error: (err) => {
        this.loading = false;
      }
    })
  }

  sendData(data: any) {
    this.userService.createUser(data).subscribe({
      next: (res) => {
        this.isOpenDialog$?.next(false);
        this.notificationService.showNotification(`Se creo exitosamente el usuario`);
        this.getListUsers();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al guardar los datos!`, 'error');
      }
    })
  }

  editUser(data: any) {
    this.userService.getUser(data.id).subscribe({
      next: (findUser) => {
        this.openDialogEdit(findUser);
      }
    })
  }

  openDialogEdit(findUser: any) {
    const formEdition: IForm = JSON.parse(JSON.stringify(this.userEditForm));
    formEdition.editForm = true;
    formEdition.dataEdit = findUser.data;
    this.isOpenDialog$?.next(true);
    const dialogRef = this.dialog.open(FormGenericComponent, {
      data: {
        title: 'Actualizar Lista de Etiquetas',
        form: formEdition,
        titleButton: 'Actualizar',
        isOpen: this.isOpenDialog$,
      },
      width: '400px',
    });
    dialogRef.componentInstance.formGeneric.subscribe({
      next: (form: FormGroup) => {
        this.formGroupUser = form;
      }
    })
    dialogRef.componentInstance.sendData.subscribe({
      next: (data: any) => {
        this.updateProdut(data);
      }
    })
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (!result) {
          this.formGroupUser?.reset();
        }
      }
    });
  }

  updateProdut(data: any) {
    this.userService.editUser(data).subscribe({
      next: (update) => {
        this.getListUsers();
        this.notificationService.showNotification(`Se actualizo exitosamente el usuario`);
        this.isOpenDialog$?.next(false);
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al actualizar los datos!`, 'error');
      }
    })
  }

  changeStatusUser(data: any, status: boolean) {
    this.userService.cahngeStatusUser(data.id, status).subscribe({
      next: (change) => {
        this.notificationService.showNotification(`Cambio exitosamente el estado del usuario`);
        this.getListUsers();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al intentar cambiar de estado!`, 'error');
      }
    })
  }

  deleteUser(data: any) {
    this.userService.deleteUser(data.id).subscribe({
      next: (resDelete) => {
        this.notificationService.showNotification(`Se elimino exitosamente el producto`, 'info');
        this.getListUsers();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al borrar el usuario!`, 'error');
      }
    })
  }

  isOpenObserver(isOpen: BehaviorSubject<boolean>) {
    this.isOpenDialog$ = isOpen;
  }

  setForm(form: FormGroup) {
    this.formGroupUser = form;
  }

}
