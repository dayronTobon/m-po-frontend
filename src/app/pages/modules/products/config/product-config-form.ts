import { ENameValidated } from '../../../../shared/components/form-generic/enum/validations.enum';
import { IForm } from '../../../../shared/components/form-generic/models/form.interface';

export const productForm: IForm = {
  fields: [
    {
      type: 'text',
      name: 'id',
      value: '',
      hidden: true,
      onlyEdit: true,
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo requerido',
        },
      ],
    },
    {
      type: 'select',
      name: 'id_category',
      label: 'Seleciona Categoria',
      value: null,
      options: [],
      method: 'GET',
      path: 'category',
      modelResponse: {
        value: 'id',
        name: 'name',
      },
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo categoria es requerido',
        },
      ],
    },
    {
      type: 'text',
      name: 'name',
      label: 'Nombre del producto',
      value: '',
      validations: [
        {
          validate: ENameValidated.required,
          message: 'Campo nombre es requerido',
        },
      ],
    },
    {
      type: 'number',
      name: 'price',
      label: 'Precio del producto',
      value: '',
    },
    {
      type: 'number',
      name: 'cost',
      label: 'Costo del producto',
      value: '',
    },
  ],
};
