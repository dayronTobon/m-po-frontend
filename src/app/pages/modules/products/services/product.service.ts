import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = environment.urlBase;

  constructor(private http: HttpClient) { }

  getListProducts(): Observable<Object> {
    return this.http.get(`${this.baseUrl}/product`);
  }

  getProduct(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/product/${id}`);
  }

  createProduct(dataProduct: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/product`, dataProduct);
  }

  editProduct(data: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/product/${data.id}`, data);
  }

  cahngeStatusProduct(id: string, status: boolean): Observable<Object> {
    return this.http.patch(`${this.baseUrl}/product/status/${id}`, { status: status});
  }

  deleteProduct(id: string): Observable<Object> {
    return this.http.delete (`${this.baseUrl}/product/${id}`);
  }

}
