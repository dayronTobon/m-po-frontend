import { Component, OnInit } from '@angular/core';
import { ProductService } from './services/product.service';
import { productForm } from './config/product-config-form';
import { BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { IActions } from '../../../shared/components/form-generic/models/actions.interface';
import { IForm } from '../../../shared/components/form-generic/models/form.interface';
import { FormGenericComponent } from '../../../shared/components/form-generic/form-generic.component';
import { MatDialog } from '@angular/material/dialog';
import { INotification } from 'src/app/shared/components/notification/models/notification.interface';
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  titleTable = 'Productos';
  columns: any[] = [];
  dataTable: any[] = [];
  loading = false;
  titleForm = 'Crear producto';
  productForm = productForm;
  formGroupProduct?: FormGroup;
  isOpenDialog$?: BehaviorSubject<boolean>;

  constructor(
    public dialog: MatDialog,
    private productService: ProductService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit(): void {
    this.getListProducts();
  }

  changeDataRow(evt: IActions) {
    const activeAction = {
      'edit': (row: any, status: boolean) => this.editProduct(row),
      'active': (row: any, status: boolean) => this.changeStatusProduct(row, status),
      'deactivate': (row: any, status: boolean) => this.changeStatusProduct(row, status),
      'delete': (row: any, status: boolean) => this.deleteProduct(row),
    }
    activeAction[evt.action]?.(evt.data, evt.action == 'active');
  }

  getListProducts() {
    this.loading = true;
    this.productService.getListProducts().subscribe({
      next: (res: any) => {
        this.loading = false;
        this.dataTable = res.data.list;
        this.columns = res.data.configColumns;
      },
      error: (err) => {
        this.loading = false;
      }
    })
  }

  sendData(data: any) {
    this.productService.createProduct(data).subscribe({
      next: (res) => {
        this.isOpenDialog$?.next(false);
        this.notificationService.showNotification(`Se creo exitosamente el producto`);
        this.getListProducts();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al guardar los datos!`, 'error');
      }
    })
  }

  editProduct(data: any) {
    this.productService.getProduct(data.id).subscribe({
      next: (findProduct) => {
        this.openDialogEdit(findProduct);
      }
    })
  }

  openDialogEdit(findProduct: any) {
    const formEdition: IForm = JSON.parse(JSON.stringify(this.productForm));
    formEdition.editForm = true;
    formEdition.dataEdit = findProduct.data;
    this.isOpenDialog$?.next(true);
    const dialogRef = this.dialog.open(FormGenericComponent, {
      data: {
        title: 'Actualizar producto',
        form: formEdition,
        titleButton: 'Actualizar',
        isOpen: this.isOpenDialog$,
      },
      width: '400px',
    });
    dialogRef.componentInstance.formGeneric.subscribe({
      next: (form: FormGroup) => {
        this.formGroupProduct = form;
      }
    })
    dialogRef.componentInstance.sendData.subscribe({
      next: (data: any) => {
        this.updateProdut(data);
      }
    })
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (!result) {
          this.formGroupProduct?.reset();
        }
      }
    });
  }

  updateProdut(data: any) {
    this.productService.editProduct(data).subscribe({
      next: (update) => {
        this.getListProducts();
        this.notificationService.showNotification(`Se actualizo exitosamente el producto`);
        this.isOpenDialog$?.next(false);
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al actualizar los datos!`, 'error');
      }
    })
  }

  changeStatusProduct(data: any, status: boolean) {
    this.productService.cahngeStatusProduct(data.id, status).subscribe({
      next: (change) => {
        this.notificationService.showNotification(`Cambio exitosamente el estado del producto`);
        this.getListProducts();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al cambiar el estado!`, 'error');
      }
    })
  }

  deleteProduct(data: any) {
    this.productService.deleteProduct(data.id).subscribe({
      next: (resDelete) => {
        this.notificationService.showNotification(`Se elimino exitosamente el producto`, 'info');
        this.getListProducts();
      },
      error: (err) => {
        this.notificationService.showNotification(`¡Ocurrio un ERROR al eliminar los datos!`, 'error');
      }
    })
  }

  isOpenObserver(isOpen: BehaviorSubject<boolean>) {
    this.isOpenDialog$ = isOpen;
  }

  setForm(form: FormGroup) {
    this.formGroupProduct = form;
  }

}
